/*
 *
 * libcoreaws3/src/AWS4RequestToCurl.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <string>

#include "common.hpp"

int main(int argc, char** argv)
{
    coreaws3::SignerHandle signer(new coreaws3::AWS4Signer());
    coreaws3::CredentialsProviderHandle credentialsProvider(
            new coreaws3::EnvironmentVariableCredentialsProvider());

    coreaws3::HttpMethod::Type method = coreaws3::HttpMethod::POST;
    coreaws3::EndpointHandle endpoint(new coreaws3::Endpoint(
            coreaws3::Scheme::https,
            "iam.amazonaws.com",
            coreaws3::Maybe<coreaws3::Port>::nothing()));
    std::string resourcePath = "/";
    coreaws3::ParameterMap parameters;
    parameters.insert(coreaws3::ParameterEntry(
            "Version", 
            coreaws3::Maybe<std::string>::just("2010-05-08")));
    parameters.insert(coreaws3::ParameterEntry(
            "Action",
            coreaws3::Maybe<std::string>::just("GetUser")));
    coreaws3::HeaderMap headers;
    coreaws3::Maybe<coreaws3::InputStreamHandle>::Type content =
            coreaws3::Maybe<coreaws3::InputStreamHandle>::nothing();

    coreaws3::RequestHandle request(new coreaws3::Request(
            method,
            endpoint,
            resourcePath,
            parameters,
            headers,
            content));

    std::cout << requestToCurl(signer,
                               credentialsProvider,
                               request) << std::endl;

    return 0;
}

