/*
 *
 * libcoreaws3/src/common.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__COMMON_HPP
#define COREAWS3__COMMON_HPP

#include <ctime>
#include <string>

#include "coreaws3/coreaws3.hpp"

std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsProviderHandle credentialsProvider,
        coreaws3::RequestHandle request,
        std::time_t time);

std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsProviderHandle credentialsProvider,
        coreaws3::RequestHandle request);

std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsHandle credentials,
        coreaws3::RequestHandle request,
        std::time_t time);

std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsHandle credentials,
        coreaws3::RequestHandle request);

std::string requestToCurl(coreaws3::RequestHandle request);

std::string httpRequestToCurl(coreaws3::HttpRequestHandle httpRequest);

std::string getInput(coreaws3::HttpRequestHandle httpRequest);

coreaws3::CredentialsHandle readCredentials();

#endif // not COREAWS3__COMMON_HPP

