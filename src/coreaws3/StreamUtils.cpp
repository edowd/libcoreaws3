/*
 *
 * libcoreaws3/src/coreaws3/StreamUtils.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "StreamUtils.hpp"

#include <cstring>
#include <fstream>
#include <sstream>
#include <stdexcept>

#include "BinaryUtils.hpp"

namespace coreaws3
{

InputStreamHandle StreamUtils::emptyInputStream()
{
    InputStreamHandle inputStream(new std::stringstream());
    return inputStream;
}

OutputStreamHandle StreamUtils::emptyOutputStream()
{
    OutputStreamHandle outputStream(new std::stringstream());
    return outputStream;
}

Buffer StreamUtils::getInputContents(InputStreamHandle inputStream)
{
    Buffer buf;

    inputStream->clear();
    inputStream->seekg(0, std::ios::beg);
    while (inputStream->good())
    {
        unsigned char c = inputStream->get();
        if (inputStream->good())
        {
            buf.push_back(c);
        }
    }
    inputStream->clear();
    inputStream->seekg(0, std::ios::beg);

    return buf;
}

std::size_t StreamUtils::getInputSize(InputStreamHandle inputStream)
{
    std::streampos cur = inputStream->tellg();
    if (cur == -1)
    {
        return -1;
    }

    inputStream->seekg(0, std::ios::end);
    std::streampos end = inputStream->tellg();
    inputStream->seekg(cur);

    return end - cur;
}

InputStreamHandle StreamUtils::openInputFile(const std::string& filename)
{
    InputStreamHandle inputStream(new std::ifstream(filename.c_str()));
    if (inputStream->fail())
    {
        std::stringstream errbuf;
        errbuf << "Error opening input file: \"" << filename << '"';
        throw std::runtime_error(errbuf.str());
    }

    return inputStream;
}

OutputStreamHandle StreamUtils::openOutputFile(const std::string& filename)
{
    OutputStreamHandle outputStream(new std::ofstream(filename.c_str()));
    if (outputStream->fail())
    {
        std::stringstream errbuf;
        errbuf << "Error opening output file: \"" << filename << '"';
        throw std::runtime_error(errbuf.str());
    }

    return outputStream;
}

InputStreamHandle StreamUtils::setInput(const Buffer& input)
{
    return StreamUtils::setInput(BinaryUtils::toString(input));
}

InputStreamHandle StreamUtils::setInput(const std::string& input)
{
    InputStreamHandle inputStream(new std::stringstream(input));
    return inputStream;
}

}

