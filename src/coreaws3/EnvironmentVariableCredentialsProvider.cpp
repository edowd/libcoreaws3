/*
 *
 * libcoreaws3/src/coreaws3/EnvironmentVariableCredentialsProvider.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "EnvironmentVariableCredentialsProvider.hpp"

#include <cstdlib>
#include <sstream>
#include <stdexcept>

#include "Credentials.hpp"

namespace coreaws3
{

EnvironmentVariableCredentialsProvider::EnvironmentVariableCredentialsProvider()
{
}

EnvironmentVariableCredentialsProvider::~EnvironmentVariableCredentialsProvider()
{
}

CredentialsHandle EnvironmentVariableCredentialsProvider::operator()()
{
    return this->refresh();
}

CredentialsHandle EnvironmentVariableCredentialsProvider::refresh()
{
    char* aWSAccessKeyId = getenv("AWS_ACCESS_KEY_ID");
    char* aWSSecretKey = getenv("AWS_SECRET_KEY");

    if (NULL == aWSAccessKeyId || NULL == aWSSecretKey)
    {
        std::stringstream errbuf;
        errbuf << "Unable to load AWS credentials from environment variables ";
        errbuf << "(AWS_ACCESS_KEY_ID and AWS_SECRET_KEY)";
        throw std::runtime_error(errbuf.str());
    }

    CredentialsHandle credentials(new Credentials(
            std::string(aWSAccessKeyId),
            std::string(aWSSecretKey)));

    return credentials;
}

}

