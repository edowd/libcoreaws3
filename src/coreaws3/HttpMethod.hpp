/*
 *
 * libcoreaws3/src/coreaws3/HttpMethod.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__HTTP_METHOD
#define COREAWS3__HTTP_METHOD

#include <string>

namespace coreaws3
{

struct HttpMethod
{
    enum Type
    {
        GET,
        POST,
        PUT,
        DELETE,
        HEAD
    };

    static Type read(const std::string& httpMethod);
    static std::string show(Type httpMethod);
};

}

#endif // not COREAWS3__HTTP_METHOD

