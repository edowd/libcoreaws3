/*
 *
 * libcoreaws3/src/coreaws3/DateTimeUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__DATE_TIME_UTILS
#define COREAWS3__DATE_TIME_UTILS

#include <ctime>
#include <string>

namespace coreaws3
{

class DateTimeUtils
{
public:
    static std::string formatCompressedISO8601(std::time_t time);
    static std::string formatDateStamp(std::time_t time);
    static std::string formatISO8601(std::time_t time);
    static std::string formatRFC822(std::time_t time);
    static std::time_t now();
private:
    static std::tm getUTC(std::time_t time);
    static std::string zeroPadded(int desiredLength, int num);
    static std::string yyyy(const std::tm& utc);
    static std::string MM(const std::tm& utc);
    static std::string dd(const std::tm& utc);
    static std::string HH(const std::tm& utc);
    static std::string mm(const std::tm& utc);
    static std::string ss(const std::tm& utc);
    static std::string EEE(const std::tm& utc);
    static std::string MMM(const std::tm& utc);
};

}

#endif // not COREAWS3__DATE_TIME_UTILS

