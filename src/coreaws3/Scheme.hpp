/*
 *
 * libcoreaws3/src/coreaws3/Scheme.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__SCHEME
#define COREAWS3__SCHEME

#include <string>

namespace coreaws3
{

struct Scheme
{
    enum Type
    {
        http,
        https
    };

    static Type read(const std::string& scheme);
    static std::string show(Type scheme);
};

}

#endif // not COREAWS3__SCHEME

