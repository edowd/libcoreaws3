/*
 *
 * libcoreaws3/src/coreaws3/Maybe.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__MAYBE
#define COREAWS3__MAYBE

#include <tr1/memory>

namespace coreaws3
{

//typedef std::tr1::shared_ptr<std::string> MaybeString;
template <typename T>
struct Maybe
{
    typedef std::tr1::shared_ptr<T> Type;

    static std::tr1::shared_ptr<T> just(T value)
    {
        return std::tr1::shared_ptr<T>(new T(value));
    }

    static std::tr1::shared_ptr<T> nothing()
    {
        return std::tr1::shared_ptr<T>();
    }
};

}

#endif // not COREAWS3__MAYBE

