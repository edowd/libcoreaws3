/*
 *
 * libcoreaws3/src/coreaws3/Credentials.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__CREDENTIALS
#define COREAWS3__CREDENTIALS

#include <string>

#include "Maybe.hpp"

namespace coreaws3
{

class Credentials
{
public:
    Credentials(const std::string& accessKeyId,
                const std::string& secretAccessKey,
                Maybe<std::string>::Type sessionToken);
    Credentials(const std::string& accessKeyId,
                const std::string& secretAccessKey);
    Credentials(const std::string& accessKeyId,
                const std::string& secretAccessKey,
                const std::string& sessionToken);
    virtual ~Credentials();

    const std::string accessKeyId;
    const std::string secretAccessKey;
    Maybe<std::string>::Type sessionToken;

private:
    Credentials(const Credentials&); // not implemented
    void operator=(const Credentials&); // not implemented
};

}

#endif // not COREAWS3__CREDENTIALS

