/*
 *
 * libcoreaws3/src/coreaws3/Endpoint.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Endpoint.hpp"

#include <sstream>

namespace coreaws3
{

Endpoint::Endpoint(Scheme::Type scheme,
                   const std::string& host,
                   Maybe<Port>::Type port) :
        scheme(scheme),
        host(host),
        port(port)
{
}

Endpoint::Endpoint(Scheme::Type scheme,
                   const std::string& host) :
        scheme(scheme),
        host(host),
        port(Maybe<Port>::nothing())
{
}

Endpoint::Endpoint(Scheme::Type scheme,
                   const std::string& host,
                   Port port) :
        scheme(scheme),
        host(host),
        port(Maybe<Port>::just(port))
{
}

Endpoint::~Endpoint()
{
}

bool Endpoint::isUsingDefaultPort() const
{
    return (!this->port ||
            (*(this->port) == 80 && this->scheme == Scheme::http) ||
            (*(this->port) == 443 && this->scheme == Scheme::https));
}

std::string Endpoint::server() const
{
    std::stringstream strbuf;

    strbuf << this->host;
    if (!isUsingDefaultPort())
    {
        strbuf << ':' << *(this->port);
    }

    return strbuf.str();
}

std::string Endpoint::toString() const
{
    std::stringstream strbuf;

    strbuf << Scheme::show(this->scheme);
    strbuf << "://";
    strbuf << this->server();

    return strbuf.str();
}

}

