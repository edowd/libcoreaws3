/*
 *
 * libcoreaws3/src/coreaws3/AWS3Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__AWS_3_SIGNER
#define COREAWS3__AWS_3_SIGNER

#include "HeaderEntry.hpp"
#include "HeaderMap.hpp"
#include "Signer.hpp"

namespace coreaws3
{

class AWS3Signer : public Signer
{
public:
    static RequestHandle sign(CredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time);

    AWS3Signer();
    virtual ~AWS3Signer();
    virtual RequestHandle operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const;

private:
    static std::string stringToSign(RequestHandle request);
    static std::string canonicalHeaders(const HeaderMap& lowerHeaders);
    static std::string buildCanonicalHeaders(const std::string& acc,
                                             const HeaderEntry& header);
    static HeaderMap interestingHeaders(const HeaderMap& lowerHeaders);
    static bool isNotInterestingHeader(const HeaderEntry& header);
    static std::string signedHeaders(const HeaderMap& headers);
    static std::string buildSignedHeaders(const std::string& acc,
                                          const HeaderEntry& header);
};

}

#endif // not COREAWS3__AWS_3_SIGNER

