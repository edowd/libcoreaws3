/*
 *
 * libcoreaws3/src/coreaws3/AWS2Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWS2Signer.hpp"

#include <sstream>
#include <stdexcept>

#include "DateTimeUtils.hpp"
#include "Maybe.hpp"

namespace coreaws3
{

RequestHandle AWS2Signer::sign(CredentialsHandle credentials,
                               RequestHandle request,
                               std::time_t time)
{
    ParameterMap parameters(Signer::filterSignature(request->parameters));

    ParameterMap::const_iterator ptr;
    std::string key = "AWSAccessKeyId";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (!ptr->second ||
                credentials->accessKeyId != *(ptr->second))
        {
            throw std::invalid_argument(
                    "AWSAccessKeyId does not match credentials");
        }
    }
    else
    {
        parameters.insert(ParameterEntry(
                key, Maybe<std::string>::just(credentials->accessKeyId)));
    }

    key = "SecurityToken";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if ((ptr->second &&
                credentials->sessionToken &&
                *(credentials->sessionToken) != *(ptr->second)) ||
                (ptr->second && !credentials->sessionToken) ||
                (!ptr->second && credentials->sessionToken))
        {
            throw std::invalid_argument(
                    "SecurityToken does not match credentials");
        }
    }
    else
    {
        if (credentials->sessionToken)
        {
            parameters.insert(ParameterEntry(
                    key,
                    credentials->sessionToken));
        }
    }

    std::string algorithm;
    key = "SignatureMethod";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (!ptr->second ||
                (*(ptr->second) != "HmacSHA1" &&
                *(ptr->second) != "HmacSHA256"))
        {
            throw std::invalid_argument("Invalid SignatureMethod");
        }
        algorithm = *(ptr->second);
    }
    else
    {
        algorithm = "HmacSHA256";
        parameters.insert(ParameterEntry(
                key, Maybe<std::string>::just(algorithm)));
    }

    key = "SignatureVersion";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (!ptr->second ||
                *(ptr->second) != "2")
        {
            throw std::invalid_argument("Invalid SignatureVersion");
        }
    }
    else
    {
        parameters.insert(ParameterEntry(
                key, Maybe<std::string>::just("2")));
    }

    ptr = parameters.find("Expires");
    if (ptr == parameters.end())
    {
        ptr = parameters.find("Timestamp");
        if (ptr == parameters.end())
        {
            parameters.insert(ParameterEntry(
                    "Timestamp", 
                    Maybe<std::string>::just(
                            DateTimeUtils::formatISO8601(time))));
        }
    }

    RequestHandle unsignedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            parameters,
            request->headers,
            request->content));
           
    std::string stringToSign = AWS2Signer::stringToSign(unsignedRequest);

    std::string signature = Signer::sign(algorithm,
                                         credentials->secretAccessKey,
                                         stringToSign);
    parameters.insert(ParameterEntry(
            "Signature", Maybe<std::string>::just(signature)));

    RequestHandle signedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            parameters,
            request->headers,
            request->content));

    return signedRequest;
}


AWS2Signer::AWS2Signer()
{
}

AWS2Signer::~AWS2Signer()
{
}

RequestHandle AWS2Signer::operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const
{
    return AWS2Signer::sign(credentials, request, time);
}

std::string AWS2Signer::stringToSign(RequestHandle request)
{
    std::stringstream strbuf;

    strbuf << HttpMethod::show(request->httpMethod) << '\n'
           << Signer::canonicalizedEndpoint(request->endpoint) << '\n'
           << Signer::canonicalizedResourcePath(request->resourcePath) << '\n'
           << Signer::canonicalizedQueryString(request->parameters);

    return strbuf.str();
}

}

