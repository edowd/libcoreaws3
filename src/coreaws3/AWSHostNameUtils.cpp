/*
 *
 * libcoreaws3/src/coreaws3/AWSHostNameUtils.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWSHostNameUtils.hpp"

#include "StringUtils.hpp"

namespace coreaws3 
{

std::string AWSHostNameUtils::parseRegionName(EndpointHandle endpoint)
{
    if (!StringUtils::endsWith(endpoint->host, ".amazonaws.com"))
    {
        return "us-east-1";
    }

    std::string serviceRegion = endpoint->host.substr(
            0, endpoint->host.find(".amazonaws.com"));

    std::string separator = (StringUtils::startsWith(serviceRegion, "s3")) ?
            "-"  : ".";

    std::size_t pos = serviceRegion.find(separator);
    if (pos == std::string::npos)
    {
        return "us-east-1";
    }

    std::string region = serviceRegion.substr(pos + 1);
    if (region == "us-gov")
    {
        return "us-gov-west-1";
    }
    else
    {
        return region;
    }
}

std::string AWSHostNameUtils::parseServiceName(EndpointHandle endpoint)
{
    if (!StringUtils::endsWith(endpoint->host, ".amazonaws.com"))
    {
        return "us-east-1";
    }

    std::string serviceRegion = endpoint->host.substr(
            0, endpoint->host.find(".amazonaws.com"));

    std::string separator = (StringUtils::startsWith(serviceRegion, "s3")) ?
            "-" : ".";

    std::size_t pos = serviceRegion.find(separator);
    if (pos == std::string::npos)
    {
        return serviceRegion;
    }

    std::string service = serviceRegion.substr(0, pos);

    return service;
}

}

