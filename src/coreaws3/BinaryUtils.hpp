/*
 *
 * libcoreaws3/src/coreaws3/BinaryUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__BINARY_UTILS
#define COREAWS3__BINARY_UTILS

#include <cstddef>
#include <string>

#include "Buffer.hpp"

namespace coreaws3
{

class BinaryUtils
{
public:
    static Buffer fromArray(const unsigned char* data, std::size_t size);
    static Buffer fromBase64(const std::string& data);
    static Buffer fromHex(const std::string& data);
    static Buffer fromString(const std::string& str);
    static std::string toBase64(const Buffer& data);
    static std::string toHex(const Buffer& data);
    static std::string toString(const Buffer& data);
private:
    static std::string buildHex(const std::string& acc, unsigned char c);
};

}

#endif // not COREAWS3__BINARY_UTILS

