/*
 *
 * libcoreaws3/src/coreaws3/AWS4Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWS4Signer.hpp"

#include <numeric>
#include <sstream>

#include "AWSHostNameUtils.hpp"
#include "BinaryUtils.hpp"
#include "Buffer.hpp"
#include "DateTimeUtils.hpp"
#include "HashUtils.hpp"
#include "Maybe.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws3
{

RequestHandle AWS4Signer::sign(CredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time)
{
    HeaderMap headers(Signer::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    std::string service = AWSHostNameUtils::parseServiceName(request->endpoint);
    std::string region = AWSHostNameUtils::parseRegionName(request->endpoint);

    std::string algorithm = "AWS4-HMAC-SHA256";
    std::string terminator = "aws4_request";

    if (lowerHeaders.find("host") == lowerHeaders.end())
    {
        std::string key = "Host";
        Maybe<std::string>::Type value =
                Maybe<std::string>::just(request->endpoint->server());
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    std::string dateStamp = DateTimeUtils::formatDateStamp(time);
    std::string compressedISO8601 =
            DateTimeUtils::formatCompressedISO8601(time);

    if (lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "X-Amz-Date";
        Maybe<std::string>::Type value =
                Maybe<std::string>::just(compressedISO8601);
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (credentials->sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        Maybe<std::string>::Type value = credentials->sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    RequestHandle unsignedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    std::string canonicalRequest =
            AWS4Signer::canonicalRequest(unsignedRequest);
    std::string scope =
            AWS4Signer::scope(dateStamp, region, service, terminator);
    std::string signingCredentials =
            AWS4Signer::signingCredentials(credentials, scope);
    std::string stringToSign = AWS4Signer::stringToSign(algorithm,
                                                        compressedISO8601,
                                                        scope,
                                                        canonicalRequest);
    Buffer signature = AWS4Signer::signature(credentials,
                                             dateStamp,
                                             region,
                                             service,
                                             terminator,
                                             stringToSign);
    std::string authorizationHeaderValue = AWS4Signer::authorizationHeaderValue(
            algorithm,
            signingCredentials,
            AWS4Signer::signedHeaders(lowerHeaders),
            signature);

    headers.insert(HeaderEntry(
            "Authorization",
            Maybe<std::string>::just(authorizationHeaderValue)));
                                              
    RequestHandle signedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    return signedRequest;
}

AWS4Signer::AWS4Signer()
{
}

AWS4Signer::~AWS4Signer()
{
}

RequestHandle AWS4Signer::operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const
{
    return AWS4Signer::sign(credentials, request, time);
}

std::string AWS4Signer::canonicalRequest(RequestHandle request)
{
    HeaderMap lowerHeaders(Signer::lowerCaseKey(request->headers));

    std::size_t contentSize = (request->content) ? 
            StreamUtils::getInputSize(*(request->content)) : 0;
    std::string canonicalQueryString =
            Signer::canonicalizedQueryString(request->parameters);

    std::string queryString;
    InputStreamHandle content;
    if (request->httpMethod != HttpMethod::POST || contentSize != 0)
    {
        queryString = canonicalQueryString;
        content = (request->content) ?
                *(request->content) : StreamUtils::emptyInputStream();
    }
    else
    {
        queryString = "";
        content = StreamUtils::setInput(canonicalQueryString);
    }

    std::stringstream canonicalRequest;
    canonicalRequest << HttpMethod::show(request->httpMethod) << '\n';
    canonicalRequest << 
            Signer::canonicalizedResourcePath(request->resourcePath) << '\n';
    canonicalRequest << queryString << '\n';
    canonicalRequest << AWS4Signer::canonicalHeaders(lowerHeaders) << '\n';
    canonicalRequest << AWS4Signer::signedHeaders(lowerHeaders) << '\n';
    canonicalRequest << AWS4Signer::payloadHash(content);

    return canonicalRequest.str();
}

std::string AWS4Signer::canonicalHeaders(const HeaderMap& lowerHeaders)
{
    return std::accumulate(lowerHeaders.begin(),
                           lowerHeaders.end(),
                           std::string(),
                           AWS4Signer::buildCanonicalHeaders);
}

std::string AWS4Signer::buildCanonicalHeaders(const std::string& acc,
                                              const HeaderEntry& header)
{
    std::stringstream strbuf;

    strbuf << acc;
    strbuf << header.first;
    strbuf << ':';
    if (header.second)
    {
        strbuf << *(header.second);
    }
    strbuf << '\n';
    return strbuf.str();
}

std::string AWS4Signer::signedHeaders(const HeaderMap& lowerHeaders)
{
    std::string signedHeadersString = std::accumulate(
            lowerHeaders.begin(),
            lowerHeaders.end(),
            std::string(),
            AWS4Signer::buildSignedHeaders);

    if (!signedHeadersString.empty())
    {
        return signedHeadersString.substr(1);
    }
    else
    {
        return signedHeadersString;
    }
}

std::string AWS4Signer::buildSignedHeaders(const std::string& acc,
                                           const HeaderEntry& header)
{
    return acc + std::string(";") + header.first;
}

std::string AWS4Signer::payloadHash(InputStreamHandle payload)
{
    Buffer inputBuffer = StreamUtils::getInputContents(payload);

    return BinaryUtils::toHex(HashUtils::hash("sha256", inputBuffer));
}

std::string AWS4Signer::scope(const std::string& dateStamp,
                              const std::string& region,
                              const std::string& service,
                              const std::string& terminator)
{
    std::stringstream strbuf;
    strbuf << dateStamp << '/' << region << '/' << service << '/' << terminator;
    return strbuf.str();
}

std::string AWS4Signer::signingCredentials(CredentialsHandle credentials,
                                           const std::string& scope)
{
    return credentials->accessKeyId + std::string("/") + scope;
}

std::string AWS4Signer::stringToSign(const std::string& algorithm,
                                     const std::string& iso8601,
                                     const std::string& scope,
                                     const std::string& canonicalRequest)
{
    std::stringstream strbuf;
    strbuf << algorithm << '\n';
    strbuf << iso8601 << '\n';
    strbuf << scope << '\n';
    strbuf << BinaryUtils::toHex(
            HashUtils::hash("sha256",
                            BinaryUtils::fromString(canonicalRequest)));
    return strbuf.str();
}

Buffer AWS4Signer::signature(CredentialsHandle credentials,
                             const std::string& dateStamp,
                             const std::string& region,
                             const std::string& service,
                             const std::string& terminator,
                             const std::string& stringToSign)
{
    return HashUtils::hmacHash(
            "sha256",
            AWS4Signer::kSigning(credentials,
                                 dateStamp,
                                 region,
                                 service,
                                 terminator),
            BinaryUtils::fromString(stringToSign));
}

Buffer AWS4Signer::kSigning(CredentialsHandle credentials,
                            const std::string& dateStamp,
                            const std::string& region,
                            const std::string& service,
                            const std::string& terminator)
{
    return HashUtils::hmacHash(
            "sha256",
            AWS4Signer::kService(credentials, dateStamp, region, service),
            BinaryUtils::fromString(terminator));
}

Buffer AWS4Signer::kService(CredentialsHandle credentials,
                            const std::string& dateStamp,
                            const std::string& region,
                            const std::string& service)
{
    return HashUtils::hmacHash(
            "sha256",
            AWS4Signer::kRegion(credentials, dateStamp, region),
            BinaryUtils::fromString(service));
}

Buffer AWS4Signer::kRegion(CredentialsHandle credentials,
                           const std::string& dateStamp,
                           const std::string& region)
{
    return HashUtils::hmacHash(
            "sha256",
            AWS4Signer::kDate(credentials, dateStamp),
            BinaryUtils::fromString(region));
}

Buffer AWS4Signer::kDate(CredentialsHandle credentials,
                         const std::string& dateStamp)
{
    return HashUtils::hmacHash(
            "sha256",
            AWS4Signer::kSecret(credentials),
            BinaryUtils::fromString(dateStamp));
}

Buffer AWS4Signer::kSecret(CredentialsHandle credentials)
{
    return BinaryUtils::fromString(
            std::string("AWS4") + credentials->secretAccessKey);
}

std::string AWS4Signer::authorizationHeaderValue(
        const std::string& algorithm,
        const std::string& signingCredentials,
        const std::string& signedHeaders,
        const Buffer& signature)
{
    std::stringstream strbuf;
    strbuf << algorithm << ' '
           << "Credential=" << signingCredentials << ", "
           << "SignedHeaders=" << signedHeaders << ", "
           << "Signature=" << BinaryUtils::toHex(signature);
    return strbuf.str();
}

}

