/*
 *
 * libcoreaws3/src/coreaws3/Request.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__REQUEST
#define COREAWS3__REQUEST

#include <string>

#include "EndpointHandle.hpp"
#include "HttpMethod.hpp"
#include "InputStreamHandle.hpp"
#include "Maybe.hpp"
#include "ParameterMap.hpp"
#include "HeaderMap.hpp"

namespace coreaws3
{

class Request
{
public:
    Request(HttpMethod::Type httpMethod,
            EndpointHandle endpoint,
            const std::string& resourcePath,
            const ParameterMap& parameters,
            const HeaderMap& headers,
            Maybe<InputStreamHandle>::Type content);

    Request(HttpMethod::Type httpMethod,
            EndpointHandle endpoint,
            const std::string& resourcePath,
            const ParameterMap& parameters,
            const HeaderMap& headers,
            InputStreamHandle content);

    Request(HttpMethod::Type httpMethod,
            EndpointHandle endpoint,
            const std::string& resourcePath,
            const ParameterMap& parameters,
            const HeaderMap& headers);

    virtual ~Request();

    const HttpMethod::Type httpMethod;
    const EndpointHandle endpoint;
    const std::string resourcePath;
    const ParameterMap parameters;
    const HeaderMap headers;
    const Maybe<InputStreamHandle>::Type content;

private:
    Request(const Request&); // not implemented
    void operator=(const Request&); // not implemented
};

}

#endif // not COREAWS3__REQUEST

