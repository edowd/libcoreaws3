/*
 *
 * libcoreaws3/src/coreaws3/S3QueryStringSigner.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "S3QueryStringSigner.hpp"

#include <sstream>
#include <stdexcept>

namespace coreaws3
{

RequestHandle S3QueryStringSigner::sign(CredentialsHandle credentials,
                                        RequestHandle request,
                                        std::time_t expires)
{
    if (credentials->sessionToken)
    {
        throw std::invalid_argument(
                "Cannot sign S3 Query String with Session Credentials");
    }

    ParameterMap parameters(Signer::filterSignature(request->parameters));
    HeaderMap headers(Signer::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    parameters.insert(ParameterEntry(
            "AWSAccessKeyId",
            Maybe<std::string>::just(credentials->accessKeyId)));
    std::stringstream expiresStrbuf;
    expiresStrbuf << expires;
    std::string expiresStr = expiresStrbuf.str();
    parameters.insert(ParameterEntry("Expires",
                                     Maybe<std::string>::just(expiresStr)));
    lowerHeaders.insert(HeaderEntry("date",
                                    Maybe<std::string>::just(expiresStr)));

    RequestHandle presignedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            parameters,
            lowerHeaders,
            request->content));

    std::string stringToSign = S3Signer::stringToSign(presignedRequest);

    std::string signature = Signer::sign("HmacSHA1",
                                         credentials->secretAccessKey,
                                         stringToSign);

    parameters.insert(ParameterEntry("Signature",
                                     Maybe<std::string>::just(signature)));

    RequestHandle signedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            parameters,
            headers,
            request->content));

    return signedRequest;
}

S3QueryStringSigner::S3QueryStringSigner()
{
}

S3QueryStringSigner::~S3QueryStringSigner()
{
}

RequestHandle S3QueryStringSigner::operator()(CredentialsHandle credentials,
                                              RequestHandle request,
                                              std::time_t expires) const
{
    return S3QueryStringSigner::sign(credentials, request, expires);
}

}

