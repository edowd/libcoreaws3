/*
 *
 * libcoreaws3/src/coreaws3/AWS3Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWS3Signer.hpp"

#include <algorithm>
#include <numeric>
#include <sstream>

#include "BinaryUtils.hpp"
#include "Buffer.hpp"
#include "DateTimeUtils.hpp"
#include "HashUtils.hpp"
#include "Maybe.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws3
{

RequestHandle AWS3Signer::sign(CredentialsHandle credentials,
                               RequestHandle request,
                               std::time_t time)
{
    HeaderMap headers(Signer::filterXAmznAuthorization(request->headers));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    if (credentials->sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        Maybe<std::string>::Type value = credentials->sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("host") == lowerHeaders.end())
    {
        std::string key = "Host";
        Maybe<std::string>::Type value =
                Maybe<std::string>::just(request->endpoint->server());
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    std::string rfc822 = DateTimeUtils::formatRFC822(time);

    if (lowerHeaders.find("date") == lowerHeaders.end())
    {
        std::string key = "Date";
        Maybe<std::string>::Type value = Maybe<std::string>::just(rfc822);
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "X-Amz-Date";
        Maybe<std::string>::Type value = Maybe<std::string>::just(rfc822);
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    RequestHandle unsignedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    std::string stringToSign = AWS3Signer::stringToSign(unsignedRequest);

    std::string algorithm = "HmacSHA256";
    std::string digest = "sha256";

    Buffer bufferToSign =
            HashUtils::hash(digest, BinaryUtils::fromString(stringToSign));
    std::string signature = Signer::sign(algorithm,
                                         credentials->secretAccessKey,
                                         bufferToSign);

    std::stringstream authHeader;
    authHeader << "AWS3 AWSAccessKeyId=" << credentials->accessKeyId
               << ",Algorithm=" << algorithm
               << ",SignedHeaders=" << AWS3Signer::signedHeaders(headers)
               << ",Signature=" << signature;

    headers.insert(HeaderEntry("X-Amzn-Authorization",
                               Maybe<std::string>::just(authHeader.str())));

    RequestHandle signedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    return signedRequest;
}

AWS3Signer::AWS3Signer()
{
}

AWS3Signer::~AWS3Signer()
{
}

RequestHandle AWS3Signer::operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const
{
    return AWS3Signer::sign(credentials, request, time);
}

std::string AWS3Signer::stringToSign(RequestHandle request)
{
    InputStreamHandle content = (request->content) ?
            *(request->content) : StreamUtils::emptyInputStream();

    HeaderMap lowerHeaders(Signer::lowerCaseKey(request->headers));
    std::stringstream strbuf;

    strbuf << HttpMethod::show(request->httpMethod) << '\n';
    strbuf << Signer::canonicalizedResourcePath(request->resourcePath) << '\n';
    strbuf << Signer::canonicalizedQueryString(request->parameters) << '\n';
    strbuf << AWS3Signer::canonicalHeaders(lowerHeaders) << '\n';
    strbuf << BinaryUtils::toString(StreamUtils::getInputContents(content));

    return strbuf.str();
}

std::string AWS3Signer::canonicalHeaders(const HeaderMap& lowerHeaders)
{
    HeaderMap interestingHeaders(AWS3Signer::interestingHeaders(lowerHeaders));
    return std::accumulate(interestingHeaders.begin(),
                           interestingHeaders.end(),
                           std::string(),
                           AWS3Signer::buildCanonicalHeaders);
}

std::string AWS3Signer::buildCanonicalHeaders(const std::string& acc,
                                              const HeaderEntry& header)
{
    std::stringstream strbuf;

    strbuf << acc;
    strbuf << header.first;
    strbuf << ':';
    if (header.second)
    {
        strbuf << *(header.second);
    }
    strbuf << '\n';
    return strbuf.str();
}

HeaderMap AWS3Signer::interestingHeaders(const HeaderMap& lowerHeaders)
{
    HeaderMap interestingHeaders;
    std::remove_copy_if(lowerHeaders.begin(),
                        lowerHeaders.end(),
                        std::inserter(interestingHeaders,
                                      interestingHeaders.end()),
                        AWS3Signer::isNotInterestingHeader);

    return interestingHeaders;
}

bool AWS3Signer::isNotInterestingHeader(const HeaderEntry& header)
{
    if (header.first == "host")
    {
        return false;
    }

    if (StringUtils::startsWith(header.first, "x-amz"))
    {
        return false;
    }

    return true;
}

std::string AWS3Signer::signedHeaders(const HeaderMap& headers)
{
    return std::accumulate(headers.begin(),
                           headers.end(),
                           std::string("Host"),
                           AWS3Signer::buildSignedHeaders);

}

std::string AWS3Signer::buildSignedHeaders(const std::string& acc,
                                           const HeaderEntry& header)
{
    std::stringstream strbuf;

    strbuf << acc;
    if (StringUtils::startsWith(StringUtils::toLowerCase(header.first),
                                "x-amz"))
    {
        strbuf << ';' << header.first;
    }

    return strbuf.str();
}

}

