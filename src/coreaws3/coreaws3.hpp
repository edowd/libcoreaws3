/*
 *
 * libcoreaws3/src/coreaws3/coreaws3.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3_HPP
#define COREAWS3_HPP

#include "AWS2Signer.hpp"
#include "AWS3HttpsSigner.hpp"
#include "AWS3Signer.hpp"
#include "AWS4Signer.hpp"
#include "AWSHostNameUtils.hpp"
#include "BinaryUtils.hpp"
#include "Buffer.hpp"
#include "CloudFrontSigner.hpp"
#include "Connection.hpp"
#include "Credentials.hpp"
#include "CredentialsHandle.hpp"
#include "CredentialsProvider.hpp"
#include "CredentialsProviderHandle.hpp"
#include "DateTimeUtils.hpp"
#include "Endpoint.hpp"
#include "EndpointHandle.hpp"
#include "EnvironmentVariableCredentialsProvider.hpp"
#include "HashUtils.hpp"
#include "HeaderEntry.hpp"
#include "HeaderMap.hpp"
#include "HttpMethod.hpp"
#include "HttpRequest.hpp"
#include "HttpRequestFactory.hpp"
#include "HttpRequestHandle.hpp"
#include "HttpUtils.hpp"
#include "InputStreamHandle.hpp"
#include "Maybe.hpp"
#include "OutputStreamHandle.hpp"
#include "ParameterEntry.hpp"
#include "ParameterMap.hpp"
#include "Port.hpp"
#include "Request.hpp"
#include "RequestHandle.hpp"
#include "Response.hpp"
#include "ResponseHandle.hpp"
#include "S3QueryStringSigner.hpp"
#include "S3Signer.hpp"
#include "Scheme.hpp"
#include "Signer.hpp"
#include "SignerHandle.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

#endif // not COREAWS3_HPP

