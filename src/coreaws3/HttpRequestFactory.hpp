/*
 *
 * libcoreaws3/src/coreaws3/HttpRequestFactory.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__HTTP_REQUEST_FACTORY
#define COREAWS3__HTTP_REQUEST_FACTORY

#include <map>
#include <string>

#include "HttpRequestHandle.hpp"
#include "HeaderEntry.hpp"
#include "HeaderMap.hpp"
#include "RequestHandle.hpp"

namespace coreaws3
{

class HttpRequestFactory
{
public:
    static HttpRequestHandle convertRequest(RequestHandle request);
    static HttpRequestHandle convertRequest(RequestHandle request,
                                            long timeout,
                                            bool verbose);
private:
    static std::size_t getContentSize(RequestHandle request);
    static std::map<std::string, std::string> processHeaders(
            const HeaderMap& headers);
    static std::pair<const std::string, std::string> processHeader(
            const HeaderEntry& header);
};

}

#endif // not COREAWS3__HTTP_REQUEST_FACTORY

